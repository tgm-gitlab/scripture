import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DungeonsAndDragonsComponent } from './dungeons-and-dragons.component';

describe('DungeonsAndDragonsComponent', () => {
  let component: DungeonsAndDragonsComponent;
  let fixture: ComponentFixture<DungeonsAndDragonsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DungeonsAndDragonsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DungeonsAndDragonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
