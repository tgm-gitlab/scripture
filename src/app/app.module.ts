import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InventoryComponent } from './components/inventory/inventory.component';
import { HorizontalTableComponent } from './components/horizontal-table/horizontal-table.component';
import { CounterComponent } from './components/counter/counter.component';
import { VerticalTableComponent } from './components/vertical-table/vertical-table.component';
import { DungeonsAndDragonsComponent } from './sheets/dungeons-and-dragons/dungeons-and-dragons.component';

@NgModule({
  declarations: [
    AppComponent,
    InventoryComponent,
    HorizontalTableComponent,
    CounterComponent,
    VerticalTableComponent,
    DungeonsAndDragonsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
