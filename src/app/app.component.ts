import { Component } from '@angular/core';
import {Pair} from './interfaces/pair';
import {EquipmentItem} from './interfaces/equipment-item';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'scripture';

  hpTotal = 23;
  hpCurrent = 17;
  statistic = [{key: 'Strength', value: 15} as Pair,
    {key: 'Intelligence', value: 21} as Pair];
  item = [{name: 'Sword', quantity: 1, weight: 6} as EquipmentItem,
    {name: 'Leather Armour', quantity: 1, weight: 12} as EquipmentItem,
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2},
    {name: 'Health Potion', quantity: 4, weight: 2}
  ];
  chars = [{key: 'Hair', value: 'Green'}, {key: 'Age', value: 43}];
}
