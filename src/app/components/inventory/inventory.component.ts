import {Component, Input} from '@angular/core';
import { EquipmentItem } from '../../interfaces/equipment-item';

@Component({
  selector: 'scripture-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent {

  @Input()
  items: EquipmentItem[] = [];

  @Input()
  name = 'Inventory';

  @Input()
  weightEnabled = false;

  @Input()
  lines = 5;

}
