import { Component, Input } from '@angular/core';
import {Pair} from '../../interfaces/pair';

@Component({
  selector: 'scripture-vertical-table',
  templateUrl: './vertical-table.component.html',
  styleUrls: ['./vertical-table.component.css']
})
export class VerticalTableComponent {
  @Input()
  name = 'Characteristics';

  @Input()
  fields: Pair[] = [];
}
