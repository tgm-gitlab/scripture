import { Component, Input } from '@angular/core';
import {Pair} from '../../interfaces/pair';

@Component({
  selector: 'scripture-horizontal-table',
  templateUrl: './horizontal-table.component.html',
  styleUrls: ['./horizontal-table.component.css']
})
export class HorizontalTableComponent {

  @Input()
  name = 'Statistics';

  @Input()
  statistics: Pair[] = [];
}
