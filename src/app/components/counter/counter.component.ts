import {Component, Input} from '@angular/core';

@Component({
    selector: 'scripture-counter',
    templateUrl: './counter.component.html',
    styleUrls: ['./counter.component.css']
})
export class CounterComponent {
    @Input()
    name = '';

    @Input()
    current = 0;

    @Input()
    total = 0;

    constructor() { }
}
