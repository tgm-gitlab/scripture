export interface Pair {
    key: string;
    value: number | string;
}
