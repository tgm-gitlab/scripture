export interface EquipmentItem {
    name: string;
    weight: number;
    quantity: number;
}
